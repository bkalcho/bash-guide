#!/bin/bash

target="/etc/"
backup="/var/backup"

if [ -d "$backup" ]; then
	mkdir "$backup"
fi

echo "Starting backup of files from $target..."
for i in /etc/*; do
	if [ -d "$i" ]; then
		cp -r "$i" "$backup"
	else
		cp "$i" "$backup"
	fi
done
echo "Backup of $target has been finished!"
