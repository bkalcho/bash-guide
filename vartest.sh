#!/bin/bash

# This script tests whether a variable is set. If not,
# it exits printing a message.

echo ${testvar:?"There is so much I still wanted to do..."}
echo "testvar is set, we can proceed."
