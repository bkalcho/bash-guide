#!/bin/bash
# Displaying just basic information.
echo "Started examing your environment..."
echo "Your home directory: $HOME"

# Displaying your terminal type
echo "You are using $TERM terminal."

echo "Your services started at runlevel 3"
ls /etc/rc3.d/S* | echo
