## Convert input lines in following form:
##	Username:Firstname:Lastname:Telephone number
## to an LDAP record in this format:
## 	dn: Username, dc=example, dc=com
##	cn: Firstname Lastname
##	sn: Lastname
##	telephoneNumber: Telephone number
##

BEGIN { FS=":" }
{ print "dn: uid=" $1 ", dc=example, dc=com\ncn: " $2, $3 "\nsn=" $3 \
	"\ntelephoneNumber: " $4 "\n" }
