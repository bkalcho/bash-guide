#!/bin/bash

# Display the name of the script being executed.
echo "${0##*/}"

# Display the first, third and tenth argument given to the script.
echo "$1 $3 ${10}"

# Display the total number of arguments passed to the script.
echo $#

# If there are more than three positional parameters, use shift to move
# all the values 3 places to the left.
if [ "$#" -gt 3 ]; then
	shift 3
fi

# Print all the values of the remaining arguments.
echo $@

# Print the number of arguments.
echo "$#"
