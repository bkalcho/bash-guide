#!/bin/bash

# This script queries user for which compression utility to use.

if [ "$#" -ne "1" ]; then
	echo "Usage: $0 file!"
fi

file="$1"

echo "Which utility would you like to use for compression?"\
	"Possible options:"
cat << LIST
gzip
bzip2
compress
zip
LIST

read utility

echo "Compressing file..."
$utility $file
echo "File has been compressed."
