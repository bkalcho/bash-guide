#!/bin/bash

echo "Fetching files from internet..."
cd $PWD
curl -L https://github.com/Automattic/simplenote-electron/releases/download/v1.1.7/Simplenote-1.1.7.tar.gz > simplenote.tar.gz

trap 'echo You can not interrupt unpacking of app!' SIGINT
echo "Unpacking files..."
tar -xzvf simplenote.tar.gz
echo "Finished installation!"
