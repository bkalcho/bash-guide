#!/bin/bash

# Check if you are executing a command as a root user

test "$(whoami)" != 'root' && { echo "you are using non-privileged account"; exit 1 ; }
