#!/bin/bash

if [ ! -f ~/log/homebackup.log ]; then
	mkdir ~/log
	touch ~/log/homebackup.log
fi

fileName="home-bkp-$(date +%Y%m%d)"
if [ -f /tmp/$fileName.tar.gz ]; then
	rm -f /tmp/$fileName.tar.gz
fi

echo "$(date +"%Y %m %d %H:%M") Started backup..." 2>&1 >>~/log/homebackup.log
tar -cvf /tmp/$fileName.tar "$HOME" 2>&1 >>~/log/homebackup.log
gzip /tmp/$fileName.tar.gz
scp /tmp/$fileName root@localhost:~ 2>&1 >>~/log/homebackup.log
echo "Log copied..." 2>&1 >>~/log/homebackup.log
