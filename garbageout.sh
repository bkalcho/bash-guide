#!/bin/bash

# Calculate the week number using the date command:

weekoffset=$[ $(date +"%V") % 2 ]

# Test if we have a remainder. If not, this is an even week so send a message.
# Else, do nothing.

if [ $weekoffset -eq "0" ]; then
	echo "Sunday evening, put out the garbage cans." |
		mail -s "Garbage cans out" root@localhost
fi
