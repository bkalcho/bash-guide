#!/bin/bash

echo "This script demonstrates function arguments."
echo

echo "Positional parameter 1 for the script is $1."
echo

test () {
	echo "Positional parameter 1 in the function is $1."
	return_value=$?
	echo "The exit code of this function is $return_value."
}

test other_param
